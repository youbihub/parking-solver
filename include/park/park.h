#pragma once
#include <array>
#include <optional>
#include <vector>

namespace park {

  struct Car {
    int length;
    std::array<int, 2> pos;  // topleft
    int alignment;           // 0=top down , 1=leftright
    std::array<int, 2> front() const {
      auto front = pos;
      front[alignment] += length - 1;
      return front;
    }
    void move_forward() { pos[alignment]++; }
    void move_backward() { pos[alignment]--; }
    auto operator<=>(const Car&) const = default;
  };

  struct Parking {
    int width;
    int height;
    std::array<int, 2> target;
  };

  struct Board {
    Parking parking;
    std::vector<Car> cars;
  };

  inline bool is_won(const std::array<int, 2>& target, const Car& car) {
    return car.front() == target;
  }
  // Board possible_next(const Board& board){};

  bool in_park(const Parking& parking, const Car& car);
  bool crash(const std::vector<Car>& cars, const int id_moving);
  bool collision(const Car& car0, const Car& car1);
  enum class movement { backward, forward };
  struct Next {
    std::vector<park::Car> cars;
    movement mov;
  };
  std::vector<Next> possible_next(const Board& board, int id_car);
  std::vector<std::pair<int, movement>> explore_board(const Board& board);
}  // namespace park
