#include <park/park.h>

#include <algorithm>
#include <iostream>
#include <optional>
#include <stack>
#include <unordered_set>

#include "xxhash.h"

using namespace park;

bool park::in_park(const Parking& parking, const Car& car) {
  auto point_in_park = [&parking](const auto pos) {
    return pos[0] >= 0 && pos[0] < parking.height && pos[1] >= 0 && pos[1] < parking.width;
  };
  auto back = point_in_park(car.pos);
  auto front = point_in_park(car.front());
  // todo: test with target
  return back && (front || car.front() == parking.target);
}
bool park::collision(const Car& car0, const Car& car1) {
  auto& a0 = car0.alignment;
  auto& a1 = car1.alignment;
  if (a0 == a1 && car0.pos[1 - a0] == car1.pos[1 - a0]) {
    if ((car0.pos[a0] <= car1.pos[a1] && car0.front()[a0] >= car1.pos[a0])
        || (car1.pos[a0] <= car0.pos[a1] && car1.front()[a1] >= car0.pos[a0])) {
      return true;
    }
  } else {
    if (a0 != a1 && car1.pos[a0] >= car0.pos[a0] && car1.pos[a0] <= car0.front()[a0]
        && car1.pos[a1] <= car0.pos[a1] && car1.front()[a1] >= car0.pos[a1]) {
      return true;
    }
  }
  return false;
}
bool park::crash(const std::vector<park::Car>& cars, const int id_car) {
  if (id_car > 0) {  // todo: needed??
    if (std::any_of(cars.begin(), cars.begin() + id_car,
                    [&](const auto& car) { return park::collision(car, cars[id_car]); })) {
      return true;
    }
  }
  if (id_car < (int)cars.size() - 1) {
    if (std::any_of(cars.begin() + id_car + 1, cars.end(),
                    [&](const auto& car) { return park::collision(car, cars[id_car]); })) {
      return true;
    }
  }
  return false;
}
// todo: possiblenext: take cars + parking as input, not boards as it forces a struct

std::vector<Next> park::possible_next(const Board& board, int id_car) {
  auto y = std::vector<Next>();
  {
    auto c = board.cars;
    c[id_car].move_backward();

    if (in_park(board.parking, c[id_car]) && !crash(c, id_car)) {
      y.push_back({c, movement::backward});
    }
  }
  {
    auto c = board.cars;
    c[id_car].move_forward();
    if (in_park(board.parking, c[id_car]) && !crash(c, id_car)) {
      y.push_back({c, movement::forward});
    }
  }
  return y;
}
namespace std {
  template <> struct hash<std::vector<park::Car>> {
    std::size_t operator()(std::vector<park::Car> const& cars) const noexcept {
      return XXH64(static_cast<const void*>(cars.data()), sizeof(Car) * cars.size(), 0);
    }
  };
}  // namespace std
struct CarTraj {
  std::vector<Car> cars;
  std::vector<std::pair<int, movement>> moves;
};
std::vector<std::pair<int, movement>> park::explore_board(const Board& board) {
  auto stack = std::stack<CarTraj>();
  auto discovered = std::unordered_set<std::vector<park::Car>>();
  stack.push({board.cars, {}});
  auto k = 0;
  while (!stack.empty()) {
    auto b = stack.top();
    stack.pop();
    if (!discovered.contains(b.cars)) {
      discovered.insert(b.cars);
      for (size_t i = 0; i < board.cars.size(); i++) {
        auto nexts = possible_next(Board{.parking = board.parking, .cars = b.cars}, i);
        if (is_won(board.parking.target, b.cars[i])) {
          std::cout << "board solved! in k =" << k << std::endl;
          return b.moves;
        }
        for (auto&& next : nexts) {
          k++;
          auto m = b.moves;
          m.push_back({i, next.mov});
          stack.push(CarTraj{next.cars, m});
        }
      }
    }
  }
  std::cout << "NO SOLUTIONS!: k=" << k << std::endl;
  return {};
}
