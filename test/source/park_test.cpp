#include <doctest/doctest.h>
#include <park/park.h>
#include <park/version.h>

#include <iostream>  //todo:remve
#include <string>

TEST_CASE("Park") {
  auto parking = park::Parking{.width = 6, .height = 6, .target = {3, 7}};
  SUBCASE("inside") {
    auto car = park::Car{.length = 3, .pos = {2, 3}, .alignment = 1};
    CHECK_EQ(park::in_park(parking, car), true);
    car = park::Car{.length = 3, .pos = {2, 3}, .alignment = 0};
    CHECK_EQ(park::in_park(parking, car), true);
    car = park::Car{.length = 3, .pos = {0, 0}, .alignment = 0};
    CHECK_EQ(park::in_park(parking, car), true);
  }
  SUBCASE("outside") {
    auto car = park::Car{.length = 3, .pos = {2, 4}, .alignment = 1};
    CHECK_EQ(park::in_park(parking, car), false);
    car = park::Car{.length = 3, .pos = {4, 3}, .alignment = 0};
    CHECK_EQ(park::in_park(parking, car), false);
  }
}

TEST_CASE("collision") {
  SUBCASE("colinear") {
    auto car1 = park::Car{.length = 2, .pos = {}, .alignment = 0};
    auto car2 = park::Car{.length = 2, .pos = {0, 1}, .alignment = 0};
    auto output = park::collision(car1, car2);
    CHECK_EQ(output, false);
    CHECK_EQ(park::collision(car1, car1), true);
    CHECK_EQ(park::collision(car2, car2), true);
    auto car3 = park::Car{.length = 3, .pos = {1, 0}, .alignment = 0};
    CHECK_EQ(park::collision(car1, car3), true);
  }
  SUBCASE("perpendicular") {
    auto car1 = park::Car{.length = 3, .pos = {1, 0}, .alignment = 1};
    auto car2 = park::Car{.length = 3, .pos = {0, 1}, .alignment = 0};
    CHECK_EQ(park::collision(car1, car2), true);
    auto car3 = park::Car{.length = 3, .pos = {0, 10}, .alignment = 0};
    CHECK_EQ(park::collision(car1, car3), false);
  }
}

TEST_CASE("Xplore board") {
  auto parking = park::Parking{6, 6, {2, 6}};
  SUBCASE("Level-01") {
    auto cars = std::vector<park::Car>();
    cars.push_back(park::Car{2, {2, 0}, 1});
    cars.push_back(park::Car{2, {3, 1}, 0});
    cars.push_back(park::Car{2, {4, 2}, 1});
    cars.push_back(park::Car{2, {3, 4}, 0});
    cars.push_back(park::Car{2, {1, 4}, 0});
    cars.push_back(park::Car{2, {0, 4}, 1});
    auto board = park::Board{parking, cars};
    auto output = park::explore_board(board);
    // for (auto &&m : output) {
    //   std::cout << "id:" << m.first
    //             << ", mov:" << (m.second == park::movement::forward ? "+1" : "-1") << std::endl;
    // }
  }
  SUBCASE("Level-750") {
    auto cars = std::vector<park::Car>();
    cars.push_back(park::Car{3, {2, 0}, 1});  // red
    cars.push_back(park::Car{2, {3, 1}, 1});  // white
    cars.push_back(park::Car{2, {5, 2}, 1});  // orange
    cars.push_back(park::Car{2, {0, 3}, 1});  // green
    cars.push_back(park::Car{2, {4, 3}, 1});  // pink
    cars.push_back(park::Car{2, {5, 4}, 1});  // blue
    cars.push_back(park::Car{3, {3, 0}, 0});  // purple1
    cars.push_back(park::Car{2, {4, 1}, 0});  // cyan
    cars.push_back(park::Car{2, {0, 2}, 0});  // blue
    cars.push_back(park::Car{2, {1, 3}, 0});  // pink
    cars.push_back(park::Car{3, {1, 4}, 0});  // purple2
    cars.push_back(park::Car{2, {0, 5}, 0});  // orange
    cars.push_back(park::Car{3, {2, 5}, 0});  // brown

    auto board = park::Board{parking, cars};
    auto output = park::explore_board(board);
    for (auto &&m : output) {
      std::cout << "id:" << m.first
                << ", mov:" << (m.second == park::movement::forward ? "+1" : "-1") << std::endl;
    }
  }
  SUBCASE("Level-347") {
    auto cars = std::vector<park::Car>();
    cars.push_back(park::Car{2, {2, 1}, 1});  // red
    cars.push_back(park::Car{2, {0, 1}, 1});  // cyan1
    cars.push_back(park::Car{2, {0, 3}, 1});  // blue
    cars.push_back(park::Car{2, {0, 3}, 1});  // cyan2
    cars.push_back(park::Car{2, {5, 1}, 1});  // white
    cars.push_back(park::Car{2, {5, 3}, 1});  // pink
    cars.push_back(park::Car{2, {4, 3}, 1});  // green

    cars.push_back(park::Car{3, {0, 0}, 0});  // brown
    cars.push_back(park::Car{2, {2, 3}, 0});  // blue
    cars.push_back(park::Car{3, {1, 3}, 0});  // purple
    cars.push_back(park::Car{2, {2, 4}, 0});  // orange
    cars.push_back(park::Car{3, {0, 5}, 0});  // white

    auto board = park::Board{parking, cars};
    auto output = park::explore_board(board);
    for (auto &&m : output) {
      std::cout << "id:" << m.first
                << ", mov:" << (m.second == park::movement::forward ? "+1" : "-1") << std::endl;
    }
  }
}